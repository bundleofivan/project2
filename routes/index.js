var express = require('express');
var router = express.Router();
var user_dal = require('../model/user_dal');


router.get('/', function(req, res, next) {
  var data = {
    title : 'Express'
  }
  if(req.session.account === undefined) {
    res.render('index', data);
  }
  else {
    data.username = req.session.account.username;
    res.render('index', data);
  }
});



router.get('/about', function(req, res, next) {
  res.render('about', { title: 'About' });
});


router.get('/login', function(req, res) {
  res.render('authentication/login.ejs');
});

router.get('/logout', function(req, res) {
  req.session.destroy( function(err) {
    res.render('authentication/logout.ejs');
  });
});

router.get('/authenticate', function(req, res) {
  user_dal.GetByUsername(req.query.username, req.query.password, function (err, account) {
    if (err) {

      res.send(err);
    }
    else if (account == null) {
      res.send("Account not found.");
    }
    else {
      req.session.account = account;
      res.send(account);
    }
  });
});

router.post('/insert_user', function(req, res) {
  console.log(req.body.username, req.body.password);
  user_dal.Insert(req.body.username, req.body.password,
      function(err){
        if(err){
          res.send('Fail!<br />' + err);
        } else {
          res.send('Success!')
        }
      });
});

router.get('/create', function(req, res) {
  res.render('userCreate.ejs');
});

module.exports = router;
