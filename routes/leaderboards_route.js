var express = require('express');
var router = express.Router();
var user_dal = require('../model/user_dal');

router.get('/', function(req, res) {
    user_dal.GetAll(function (err, result) {
            if (err) throw err;
            res.render('leaderboards.ejs', {rs: result});
        }
    );
});


router.get('/multiplayerLeaderboards', function(req, res, next) {
    user_dal.GetAllScore(function (err, result){
        if (err) {
            res.send(err);
        }
        else{
            res.render('multiplayerLeaderboards.ejs', {title: 'users', rs: result});
        }
    });

});



router.get('/campaignLeaderboards', function(req, res, next) {
    user_dal.GetAllCampaignScore(function (err, result){
        if (err) {
            res.send(err);
        }
        else{
            res.render('campaignLeaderboards.ejs', {title: 'users', rs: result});
        }
    });

});


router.get('/zombieLeaderboards', function(req, res, next) {
    user_dal.GetAllZombiesScore(function (err, result){
        if (err) {
            res.send(err);
        }
        else{
            res.render('zombieLeaderboards.ejs', {title: 'users', rs: result});
        }
    });

});





module.exports = router;