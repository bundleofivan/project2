var express = require('express');
var router = express.Router();
var user_dal = require('../model/user_dal');

router.get('/', function (req, res) {
   // console.log(req.query.user_id);
    user_dal.GetByID(req.query.user_id, function (err, result) {
            if (err) throw err;

            res.render('profile.ejs', {rs: result, user_id: req.query.user_id});
        }
    );
});


module.exports = router;
